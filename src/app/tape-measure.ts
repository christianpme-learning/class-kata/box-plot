export class TapeMeasure {
    width=0;
    height=0;
    gab=20;
    lineXPositions=new Array<number>();

    constructor(canvas: HTMLCanvasElement, legend: Array<number>){
        //..gab --width-- gab..
        this.width = canvas.width - this.gab*2;
        this.height = canvas.height/4;
        
        var context = canvas.getContext("2d");
        this.drawHorizontalLine(context);
        this.drawVerticalLines(context, legend.length);
        this.drawLegend(context, legend);
    }

    drawHorizontalLine(context: CanvasRenderingContext2D){
        context.beginPath();
        context.strokeStyle='#327fa8';
        context.moveTo(this.gab, this.height/2);
        context.lineTo(this.width+this.gab, this.height/2);
        context.stroke();
        context.beginPath();
    }

    drawVerticalLines(context: CanvasRenderingContext2D, countLines: number){
        context.beginPath();
        context.strokeStyle='#327fa8';
        let startY = this.height/2;
        let endY = this.height/2;
        let spacing = (this.width) / (countLines-1);
        var drawX = this.gab;
        for(let i=0; i<countLines; ++i){
            context.moveTo(drawX, startY-10);
            context.lineTo(drawX, endY+10);
            context.stroke();
            this.lineXPositions.push(drawX); //remember X-position

            if(i+1!=countLines){
                context.moveTo(drawX+spacing/2, startY-7);
                context.lineTo(drawX+spacing/2, endY+7);
                context.stroke();
                this.lineXPositions.push(drawX+spacing/2); //remember X-position
            }
 
            drawX = drawX + spacing;
        }
    }

    drawLegend(context: CanvasRenderingContext2D, legend: Array<number>){
        context.beginPath();
        context.strokeStyle='black';
        context.font = "17px Arial";   
        var drawX = this.gab/2;
        let spacing = (this.width) / (legend.length-1);
        legend.forEach((value)=>{
            context.fillText(value.toString(), drawX, this.height/2-20)
            drawX = drawX + spacing;
        });
    }
}