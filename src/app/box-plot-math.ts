export class BoxPlotMath {
    numberSeries: Array<number>

    constructor(numberSeries: Array<number>){
        this.numberSeries = numberSeries;
    }

    calcLowerQuartilXPosIndex(): number {
        var subArrayLowerQuartil = new Array<number>();
        this.numberSeries.forEach((value,index,array) => {
            if(this.isSubSeriesLowerQuartil(index)){
                subArrayLowerQuartil.push(value);
            }
        });
        return this.toXPosIndex(this.calcAverage(subArrayLowerQuartil));
    }

    calcUpperQuartilXPosIndex(): number {
        var subArrayUpperQuartil = new Array<number>();
        this.numberSeries.forEach((value,index,array) => {
            if(this.isSubSeriesUpperQuartil(index)){
                subArrayUpperQuartil.push(value);
            }
        });
        return this.toXPosIndex(this.calcAverage(subArrayUpperQuartil));
    }

    calcMedianXPosIndex(): number{
        var subArrayMedian = new Array<number>();
        this.numberSeries.forEach((value,index,array) => {
            if(this.isSubSeriesMedian(index)){
                subArrayMedian.push(value);
            }
        });
        return this.toXPosIndex(this.calcAverage(subArrayMedian));
    }

    isSubSeriesLowerQuartil(index: number): boolean {
        let lastIndex;
        if(this.numberSeries.length%2==0){
            lastIndex = (this.numberSeries.length/2) - 2;
        }
        else{
            lastIndex = (this.numberSeries.length/2) + 0.5 - 2;
        }
        return index<=lastIndex;
    }

    isSubSeriesUpperQuartil(index: number): boolean {
        let firstIndex;
        if(this.numberSeries.length%2==0){
            firstIndex = (this.numberSeries.length/2);
        }
        else{
            firstIndex = (this.numberSeries.length/2) - 0.5;
        }
        return index>firstIndex;
    }

    isSubSeriesMedian(index: number): boolean {
        return !this.isSubSeriesLowerQuartil(index) && !this.isSubSeriesUpperQuartil(index);
    }

    toLegendNumbers(numberSeries: Array<number>): Array<number> {
        numberSeries.sort((a, b) => a - b);
        let lastElem = numberSeries.length-1;
        let min = numberSeries[0];
        let max = numberSeries[lastElem];
        var legendNumbers: Array<number> = new Array();
        for(let element=min; element<= max; ++element){
            legendNumbers.push(element);
        }
        return legendNumbers;
    }

    calcAverage(subSeries: number[]): number {
        var sumResult=0;
        subSeries.forEach((value,index,array)=>{
          sumResult+=value;
        });
        return sumResult/subSeries.length;
    }
    
    toXPosIndex(result: number){
        let legendNumbers = this.toLegendNumbers(this.numberSeries);
        var wideRangeArray = new Array<number>();
        legendNumbers.forEach((value, index, array) => {
          wideRangeArray.push(value);
          wideRangeArray.push(value+0.5);
        });
        let roundDown=false;
        var searchIndex=wideRangeArray.findIndex((value, index, obj) => {
          if(value>=result){
            if(value>result){
                roundDown=true;
            }
            return index;
          }
        });
        if(roundDown){
            --searchIndex;
        } 
        return searchIndex;
    }
}
