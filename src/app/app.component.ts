import { Component, OnInit } from '@angular/core';
import { TapeMeasure } from './tape-measure';
import { BoxPlot } from './box-plot';
import { BoxPlotMath } from './box-plot-math';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: []
})
export class AppComponent implements OnInit {
  title = 'box-plot';
  
  ngOnInit(): void {
    var canvas = document.getElementById("measure") as HTMLCanvasElement;
    canvas.width=350;
    canvas.height=350;
  }

  onClickVisualize(numberSeriesField: string){
    var canvas = document.getElementById("measure") as HTMLCanvasElement;

    canvas.getContext("2d").clearRect(0, 0, canvas.width, canvas.height);
    
    let numberSeries = this.toNumberSeries(numberSeriesField);

    let boxPlotMath = new BoxPlotMath(numberSeries);
    let legendNumbers = boxPlotMath.toLegendNumbers(numberSeries);
    let lowerQuartilXPosIndex = boxPlotMath.calcLowerQuartilXPosIndex();
    let upperQuartilXPosIndex = boxPlotMath.calcUpperQuartilXPosIndex();
    let medianXPosIndex = boxPlotMath.calcMedianXPosIndex();

    let measure = new TapeMeasure(canvas, legendNumbers);
    let legendXPositions = measure.lineXPositions;
    let boxPlot = new BoxPlot(canvas, legendXPositions);

    boxPlot.draw(lowerQuartilXPosIndex, upperQuartilXPosIndex, medianXPosIndex);
  }

  toNumberSeries(input: string): Array<number> {
    let strings: string[] = input.split(', ');
    var values: Array<number> = new Array();
    strings.forEach((value, index) => {
      values[index] = +strings[index];
    });
    return values;
  }
}