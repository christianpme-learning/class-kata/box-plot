export class BoxPlot {
    width=0;
    height=0;
    gab=20;
    context: CanvasRenderingContext2D;
    legendXPositions: Array<number>;

    constructor(canvas: HTMLCanvasElement, legendXPositions: Array<number>){
        this.width = canvas.width;
        this.height = canvas.height/1.5;
        this.context = canvas.getContext("2d");
        this.legendXPositions = legendXPositions;
    }

    draw(lowerQuartileIndex: number, upperQuartileIndex: number, medianIndex: number){
        let lowerQuartilePosX = this.toPosX(this.legendXPositions, lowerQuartileIndex);
        let upperQuartilePosX = this.toPosX(this.legendXPositions, upperQuartileIndex);
        let medianPosX = this.toPosX(this.legendXPositions, medianIndex);
        this.drawOuterParts(this.context, lowerQuartilePosX, upperQuartilePosX);
        this.drawInnerPart(this.context, lowerQuartilePosX, upperQuartilePosX, medianPosX);
    }

    toPosX(legendXPositions: Array<number>, searchIndex: number){
        if(legendXPositions.length==searchIndex){
            return legendXPositions[searchIndex-1];
        }
        else{
            return legendXPositions[searchIndex];
        }
    }

    drawOuterParts(context: CanvasRenderingContext2D, lowerQuartilePosX: number, upperQuartilePosX: number){
        let startY = this.height/2;
        let endY = this.height/2;
        context.moveTo(this.gab, startY-20);
        context.lineTo(this.gab, endY+20);
        context.stroke();

        context.moveTo(this.gab, startY);
        context.lineTo(lowerQuartilePosX, startY);
        context.stroke();

        context.moveTo(this.width-this.gab, startY);
        context.lineTo(upperQuartilePosX, startY);
        context.stroke();

        context.moveTo(this.width-this.gab, startY-20);
        context.lineTo(this.width-this.gab, endY+20);
        context.stroke();
    }

    drawInnerPart(context: CanvasRenderingContext2D, lowerQuartilePosX: number, upperQuartilePosX: number, medianPosX: number){
        context.beginPath();
        context.strokeStyle='green';
        let rectH=50;
        let rectW=upperQuartilePosX-lowerQuartilePosX;
        context.rect(lowerQuartilePosX, (this.height/2)-rectH/2, rectW, rectH);
        context.stroke();

        context.beginPath();
        context.strokeStyle='blue';
        context.moveTo(medianPosX, (this.height/2)-rectH/2);
        context.lineTo(medianPosX, (this.height/2)+rectH/2);
        context.stroke();
    }
}