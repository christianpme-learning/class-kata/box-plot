import { BoxPlotMath } from './box-plot-math';

describe('BoxPlotMath', () => {
  it('is sub series lower quartil', () => {
    let arr = [1,2,3,4,5,6,7,8,9,10];
    let bPM = new BoxPlotMath(arr);
    expect(true).toEqual(bPM.isSubSeriesLowerQuartil(3));
  });

  it('is not sub series lower quartil', () => {
    let arr = [1,2,3,4,5,6,7,8,9,10];
    let bPM = new BoxPlotMath(arr);
    expect(false).toEqual(bPM.isSubSeriesLowerQuartil(4));
  });

  it('is sub series upper quartil', () => {
    let arr = [1,2,3,4,5,6,7,8,9,10];
    let bPM = new BoxPlotMath(arr);
    expect(true).toEqual(bPM.isSubSeriesUpperQuartil(6));
  });

  it('is not sub series upper quartil', () => {
    let arr = [1,2,3,4,5,6,7,8,9,10];
    let bPM = new BoxPlotMath(arr);
    expect(false).toEqual(bPM.isSubSeriesUpperQuartil(5));
  });

  it('is sub series median', () => {
    let arr = [1,2,3,4,5,6,7,8,9,10];
    let bPM = new BoxPlotMath(arr);
    expect(true).toEqual(bPM.isSubSeriesMedian(5));
  });

  it('is not sub series median', () => {
    let arr = [1,2,3,4,5,6,7,8,9,10];
    let bPM = new BoxPlotMath(arr);
    expect(false).toEqual(bPM.isSubSeriesMedian(6));
  });

  it(`calculate lower quartil`, () => {
    let arr = [1,2,3,4,5,6,7,8,9,10];
    let bPM = new BoxPlotMath(arr);
    expect(18).toEqual(bPM.calcAverage([17,18,18,19]));
  });

  it(`calculate upper quartil`, () => {
    let arr = [1,2,3,4,5,6,7,8,9,10];
    let bPM = new BoxPlotMath(arr);
    expect(24.25).toEqual(bPM.calcAverage([24,24,24,25]));
  });

  it(`to legend numbers`, () => {
    let arr = [1,2,3,4,5,6,7,8,9,10];
    let bPM = new BoxPlotMath(arr);
    expect([17, 18, 19, 20, 21, 22, 23, 24, 25]).toEqual(bPM.toLegendNumbers([17, 18, 18, 19, 19, 20, 24, 24, 24, 25]));
  });

  it(`bug fix: is sub series lower quartil length%2!=0`, () => {
    let arr = [18, 19, 19, 20, 24];
    let bPM = new BoxPlotMath(arr);
    expect(true).toEqual(bPM.isSubSeriesLowerQuartil(0));
    expect(true).toEqual(bPM.isSubSeriesLowerQuartil(1));
    expect(false).toEqual(bPM.isSubSeriesLowerQuartil(2));
  });

  it(`bug fix: is sub series upper quartil length%2!=0`, () => {
    let arr = [17, 18, 18, 19, 19];
    let bPM = new BoxPlotMath(arr);
    expect(false).toEqual(bPM.isSubSeriesUpperQuartil(2));
    expect(true).toEqual(bPM.isSubSeriesUpperQuartil(3));
    expect(true).toEqual(bPM.isSubSeriesUpperQuartil(4));
  });
});
